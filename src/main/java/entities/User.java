package entities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import utils.UserUtil;

public class User {
	WebDriver driver;
	By useremail=By.id(UserUtil.USERMAIL_INPUT);
	By password= By.id(UserUtil.PASSWORD_INPUT);
	By signInButton= By.xpath(UserUtil.SIGNIN_BUTTON);
	By navSignIn= By.xpath(UserUtil.NAVBAR_SIGNIN);
	By navMyTasks= By.xpath(UserUtil.NAVBAR_MYTASKS);

	public User(WebDriver driver)
	{
		this.driver=driver;
	}
	public void typeEmail()
	{
		driver.findElement(useremail).sendKeys(UserUtil.USERMAIL_VALUE);
	}
	public void typePassword()
	{
		driver.findElement(password).sendKeys(UserUtil.USERPASSWORD_VALUE);
		
	}
	public void clickLoginButton()
	{
		driver.findElement(signInButton).click();
		
	}
	public void navSignInByXpath()
	{
		driver.findElement(navSignIn).click();
		
	}
	public void navMyTasksByXpath()
	{
		driver.findElement(navMyTasks).click();
		
	}
}
