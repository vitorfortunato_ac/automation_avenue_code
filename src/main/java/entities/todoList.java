package entities;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import utils.TodoUtil;

public class TodoList {
	WebDriver driver;
	By taskDescription=By.id(TodoUtil.TODO_DESCRIPTION_INPUT);
	By addButton= By.xpath(TodoUtil.TODO_INSERT_BUTTON);


	public TodoList(WebDriver driver)
	{
		this.driver=driver;
	}
	public void typeDescription(String description)
	{
		driver.findElement(taskDescription).sendKeys(description);
	}
	public void clickAddButton()
	{
		driver.findElement(addButton).click();
	}

}
