package cucumber;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
//suite teste runner
@RunWith(Cucumber.class)
@CucumberOptions(
		format= {"pretty","json:target/"},
		features= {"src/test/java/cucumber/features"},
		glue="src/test/java/cucumber/stepdefs")

public class CucumberRunner {

}
