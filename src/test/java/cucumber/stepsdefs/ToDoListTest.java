package cucumber.stepsdefs;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import entities.TodoList;
import entities.User;
import utils.MessagesUtil;
import utils.TodoUtil;
import utils.UrlUtil;
import utils.UserUtil;

public class ToDoListTest {
	private WebDriver driver;
	private User user;
	private TodoList todoList;
	private String bodyText;

	/*
	 * Before start the test we should star browser, Signin on our application
	 * and validate that we are signedin
	 */
	@Before
	public void beforeScenarioToLogin() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "drivers\\chromedriver.exe");
		// Launch Chrome
		driver = new ChromeDriver();
		// Maximize window
		driver.manage().window().maximize();
		// Open the web page
		driver.get(UrlUtil.URL);
		// Start our entities
		user = new User(driver);
		todoList = new TodoList(driver);
		Thread.sleep(2000);
		// Go to Signin page using Xpath
		user.navSignInByXpath();
		Thread.sleep(2000);
		// Type information to signin and click to signin
		user.typeEmail();
		user.typePassword();
		user.clickLoginButton();
		Thread.sleep(1000);
		// Validate if user has logged in correctly
		bodyText = driver.findElement(By.tagName("body")).getText();
		Assert.assertTrue(MessagesUtil.MESSAGE_SUCCES_LOGIN, bodyText.contains(MessagesUtil.MESSAGE_SUCCES_LOGIN));
		// Go to my task page
		user.navMyTasksByXpath();
		Thread.sleep(2000);

	}

	// Just after of all tests, close our broswe
	@After
	public void afterScenario() throws InterruptedException {

		driver.quit();
	}
	// Validate if have sign out button on page body (if we are logged in)

	@Given("^that I need to be logged in$")
	public void validateLoggedIn() throws Throwable {
		String body = driver.findElement(By.tagName("body")).getText();
		Assert.assertTrue(MessagesUtil.MESSAGE_VERIFY_LOGGEDIN, body.contains(MessagesUtil.MESSAGE_VERIFY_LOGGEDIN));
	}

	// Verify if the user it's on the page
	@Given("^I'm on taks page$")
	public void validateIfInTaskPage() throws Throwable {
		Assert.assertTrue(UrlUtil.TASK_URL, driver.getCurrentUrl().contains(UrlUtil.TASK_URL));
	}

	// Validate if the message it's correctly displayed
	@Then("^I will validate the top message$")
	public void validateTopMessage() throws Throwable {
		String body = driver.findElement(By.tagName("body")).getText();
		// Our system don't work correctly, when bug fixed, change o assertTrue
		Assert.assertFalse(
				MessagesUtil.MESSAGE_CHECK_PART_ONE + UserUtil.USERNAME + MessagesUtil.MESSAGE_CHECK_PART_TWO,
				body.contains(
						MessagesUtil.MESSAGE_CHECK_PART_ONE + UserUtil.USERNAME + MessagesUtil.MESSAGE_CHECK_PART_TWO));
	}

	// Try to save a task with more than 250 characters
	@Given("^I try input a task that have more than characters$")
	public void createTaskWithMoreThanTwentyFifhtyCharacters() throws Throwable {
		todoList.typeDescription(TodoUtil.TODO_VALUE_MORE);
		Thread.sleep(2000);

		todoList.clickAddButton();

	}

	// Check if we have saved a task with more than 250 character
	@Then("^it's will be not allowed$")
	public void i_receive_a_message_showing_that_is_not_possible() throws Throwable {
		// Go to the first item on the list and check his description size
		int size = driver.findElement(By.xpath(TodoUtil.TODO_FIRST_ITEM)).getText().length();
		// Check if first item is the correct item and check if he is bigger
		// than 250
		if (size > 250
				&& driver.findElement(By.xpath(TodoUtil.TODO_FIRST_ITEM)).getText().equals(TodoUtil.TODO_VALUE_MORE)) {
			Thread.sleep(2000);
			// Back to same state before start test, deleting our data
			driver.findElement(By.xpath(TodoUtil.TODO_DELETE_BUTTON)).click();
			// Throw new excpetion if this fail, if not, pass
			throw new Exception("Fail, we shouldn`t able to insert this task - Is too long");
		}

	}

	// Try to save a task with less than 3 character
	@When("^I try input a task that less than$")
	public void createTaskLessThanThreeCharacter() throws Throwable {
		todoList.typeDescription(TodoUtil.TODO_VALUE_LESS);
		Thread.sleep(2000);

		todoList.clickAddButton();
	}

	@Then("^shouldn't be possible$")
	public void validateErrorLessThanThreeCharacters() throws Throwable {
		// Go to the first item on the list and check his description size

		int size = driver.findElement(By.xpath(TodoUtil.TODO_FIRST_ITEM)).getText().length();
		// Check if first item is the correct item and check if it's less than 3
		if (size < 3
				&& driver.findElement(By.xpath(TodoUtil.TODO_FIRST_ITEM)).getText().equals(TodoUtil.TODO_VALUE_LESS)) {
			Thread.sleep(2000);
			// Back to same state before start test, deleting our data
			driver.findElement(By.xpath(TodoUtil.TODO_DELETE_BUTTON)).click();
			Thread.sleep(2000);
			// Throw new excpetion if this fail, if not, pass
			throw new Exception("Fail, we shouldn`t able to insert this task - Is too short");
		}

	}

	@When("^I try input a task between three and two hundred and fifty$")
	public void saveARegularTask() throws Throwable {
		todoList.typeDescription(TodoUtil.TODO_VALUE_REGULAR);
		Thread.sleep(2000);

		todoList.clickAddButton();
	}

	@When("^and will be save$")
	public void validateIfSaved() throws Throwable {
		// Go to the first item on the list and check his description size

		int size = driver.findElement(By.xpath(TodoUtil.TODO_FIRST_ITEM)).getText().length();
		if (size > 3 && size < 251 && driver.findElement(By.xpath(TodoUtil.TODO_FIRST_ITEM)).getText()
				.equals(TodoUtil.TODO_VALUE_REGULAR)) {
			Thread.sleep(2000);
			// Back to same state before start test, deleting our data
			driver.findElement(By.xpath(TodoUtil.TODO_DELETE_BUTTON)).click();
			Thread.sleep(2000);

		} else {
			// Throw new excpetion if this fail, if not, pass
			throw new Exception("Error, data didn`t correctly saved");

		}

	}

}
