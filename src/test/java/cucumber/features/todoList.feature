# language: en

Feature: Vitor ToDoList
			As a ToDo App user
			I should be able to create a task
			So I can manage my tasks

	Background: User is Logged In
    Given that I need to be logged in
    And I'm on taks page
    
  Scenario: Validate message on the top part

    Then I will validate the top message
   Scenario: Validate that taks can`t have more than characters

		When I try input a task that have more than characters
		Then it's will be not allowed

		Scenario: Validate that tasks should require at least three characters.

    When I try input a task that less than
    Then shouldn't be possible
    
    Scenario: Validate to enter new task by clicking on the add task button
    When I try input a task between three and two hundred and fifty
    And and will be save
    
