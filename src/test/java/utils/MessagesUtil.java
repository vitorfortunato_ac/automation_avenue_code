package utils;

public class MessagesUtil {
	public static final String MESSAGE_SUCCES_LOGIN = ReadProperties.readPropertiesMessages("succes.login");

	public static final String MESSAGE_VERIFY_LOGGEDIN = ReadProperties.readPropertiesMessages("verify.if.logedin");
	public static final String MESSAGE_CHECK_PART_ONE = ReadProperties.readPropertiesMessages("check.message.top.part.one");
	public static final String MESSAGE_CHECK_PART_TWO = ReadProperties.readPropertiesMessages("check.message.top.part.two");

}
