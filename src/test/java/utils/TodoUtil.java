package utils;

public class TodoUtil {
	public static final String TODO_INSERT_BUTTON = ReadProperties.readPropertiesToDo("todo.button.input");
	public static final String TODO_DESCRIPTION_INPUT = ReadProperties.readPropertiesToDo("todo.descriptiontask.input");

	public static final String TODO_FIRST_ITEM = ReadProperties.readPropertiesToDo("list.firt.item.ref");

	public static final String TODO_DELETE_BUTTON = ReadProperties.readPropertiesToDo("delete.button.input");

	public static final String TODO_VALUE_LESS = ReadProperties.readPropertiesToDo("todo.lessthanthree.value");
	public static final String TODO_VALUE_MORE = ReadProperties.readPropertiesToDo("todo.morethantwohundredfifthy.value");
	public static final String TODO_VALUE_REGULAR = ReadProperties.readPropertiesToDo("todo.regular.value");



}
