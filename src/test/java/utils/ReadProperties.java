package utils;

import java.io.FileInputStream;
import java.util.Properties;

public class ReadProperties {
	/**
	 * 
	 * 
	 * @param nameProprertie
	 * @return
	 */
	public static String readPropertiesLogin(String nameProprertie) {
		String valorPropriedade = null;

		try {
			Properties properties = new Properties();
			properties.load(new FileInputStream("src/test/resources/properties/login/login.properties"));

			valorPropriedade = properties.getProperty(nameProprertie);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return valorPropriedade;
	}
	public static String readPropertiesMessages(String nameProprertie) {
		String valorPropriedade = null;

		try {
			Properties properties = new Properties();
			properties.load(new FileInputStream("src/test/resources/properties/entities/messages.properties"));

			valorPropriedade = properties.getProperty(nameProprertie);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return valorPropriedade;
	}
	public static String readPropertiesUser(String nameProprertie) {
		String valorPropriedade = null;

		try {
			Properties properties = new Properties();
			properties.load(new FileInputStream("src/test/resources/properties/entities/user.properties"));

			valorPropriedade = properties.getProperty(nameProprertie);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return valorPropriedade;
	}

	public static String readPropertiesToDo(String nameProprertie) {
		String valorPropriedade = null;

		try {
			Properties properties = new Properties();
			properties.load(new FileInputStream("src/test/resources/properties/entities/todo.properties"));

			valorPropriedade = properties.getProperty(nameProprertie);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return valorPropriedade;
	}
}
