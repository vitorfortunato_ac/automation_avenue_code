package utils;

public class UserUtil {
	public static final String USERMAIL_INPUT = ReadProperties.readPropertiesUser("useremail.input");
	public static final String PASSWORD_INPUT= ReadProperties.readPropertiesUser("password.input");
	public static final String SIGNIN_BUTTON = ReadProperties.readPropertiesUser("signInButton");
	public static final String NAVBAR_SIGNIN = ReadProperties.readPropertiesUser("navSignIn");
	public static final String NAVBAR_MYTASKS = ReadProperties.readPropertiesUser("navMyTasks");
	public static final String USERNAME = ReadProperties.readPropertiesUser("username");
	public static final String USERMAIL_VALUE= ReadProperties.readPropertiesUser("useremail.value");
	public static final String USERPASSWORD_VALUE = ReadProperties.readPropertiesUser("password.value");

}
